package security09;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;



/**
 *  09相对于08 就新增了这一个注解，开启后将session存储在redis中
 *  @EnableRedisHttpSession 是Spring框架中的一个注解，用于将HTTP会话数据（Session和SessionId等）存储在Redis中，而不是默认的内存中。
 * **/
@EnableRedisHttpSession
@SpringBootApplication
@MapperScan("security09.mapper")
public class Security091Application {

	public static void main(String[] args) {
		SpringApplication.run(Security091Application.class, args);
	}

}


