package security41.jwt.service;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import security41.jwt.model.TMenu;
import security41.jwt.model.TRole;
import security41.jwt.model.TUser;

import java.util.ArrayList;
import java.util.List;

@Service
public class TUserService {

    public static final long EXPIRY = 60L * 60 * 24;

    public TUser selectLoginUserInfoByUsername(String username) {

        TUser user = new TUser();
        user.setId(1L);
        user.setUsername(username);

        //user.setPassword("{bcrypt}" + new BCryptPasswordEncoder().encode("123456"));
        user.setPassword(new BCryptPasswordEncoder().encode("123456"));

        List<TMenu> menuList = new ArrayList<>();
        TMenu menu = new TMenu();
        menu.setId(1L);
        menu.setAuthority("app");
        menuList.add(menu);

        TMenu menu1 = new TMenu();
        menu1.setId(2L);
        menu1.setAuthority("guoguo");
        menuList.add(menu1);

        user.setAuthorities(menuList);

        return user;
    }


    public TUser selectUserByUsername(String username) {
        TUser user = new TUser();
        user.setId(1L);
        user.setUsername(username);
        return user;
    }


    public String getCurrentUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }


    public TUser getCurrentUser() {
        String currentUsername = getCurrentUsername();
        return selectUserByUsername(currentUsername);
    }
}
