package security41.jwt.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TRole {

    private Long id;

    private String name;

    private List<TUser> userList = new ArrayList<>();

    private List<TMenu> menuList = new ArrayList<>();

}
