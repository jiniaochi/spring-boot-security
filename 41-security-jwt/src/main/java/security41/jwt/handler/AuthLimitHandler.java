package security41.jwt.handler;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import security41.jwt.common.Results;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 权限不足 拒绝访问 handler
 */
@Slf4j
public class AuthLimitHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        log.error("没有权限访问该URL,{}", e.getMessage(), e);

        response.setContentType("application/json;charset=UTF-8");
        Results results = new Results();
        results.setCode(403);
        results.setMsg("没有权限访问该URL");
        response.getWriter().write(JSON.toJSONString(results));
    }

}

