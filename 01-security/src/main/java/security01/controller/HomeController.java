package security01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    // 默认使用 session-cookie授权方案，且cookie有效期为session; 这里使用默认的登录页
    @GetMapping("/")
    public String home(){
        return "home";
    }
}

