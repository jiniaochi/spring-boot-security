package security06.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdController {

    // 不登陆也可以访问
    @GetMapping("/ad/hello")
    public String ad(){
        return "ad";
    }
}
