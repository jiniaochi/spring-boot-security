# JWT 登录

注： 没有登录页，只能用postman测试

## 1. 设置session为无状态

`http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);`

## 2. 添加额外的过滤器: JWT验证器

`http.addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);`

