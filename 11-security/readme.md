# 菜单权限功能

用户、角色、菜单（以及中间表 用户-角色表，用户-菜单表）三张表，完成授权、校验。

## 1. 查询当前url所需要的角色

实现 `ObjectPostProcessor<FilterSecurityInterceptor>` 查询url所需要的角色。


## 2. 查询当前用户的角色列表，并判断是否匹配

返回一个 `AbstractAccessDecisionManager`抽象类的实现对象, 如下面的`UnanimousBased`就是其实现类。

```java
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
    //...
    
    private AccessDecisionManager accessDecisionManager() {
        List<AccessDecisionVoter<? extends Object>> decisionVoters = new ArrayList<>();
        decisionVoters.add(new WebExpressionVoter());
        decisionVoters.add(new AuthenticatedVoter());
        decisionVoters.add(new RoleVoter());
        /* 路由权限管理 */
        decisionVoters.add(new UrlRoleAuthHandler());

        return new UnanimousBased(decisionVoters); // 每个投票者选择弃权或同意则允许访问
    }   
}
```
