package security11.model;

import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;

@ToString
public class SysRole implements GrantedAuthority {

    private Long id;
    private String role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role;
    }
}



